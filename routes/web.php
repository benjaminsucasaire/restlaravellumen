<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


/*llamos al controlador y a asu funcion mendiante :OficinaController@index, la cual sera llamados desde el navegador con /oficina  */
$router->get('/oficina','OficinaController@index');

$router->get('/oficina/{id}','OficinaController@ver');

$router->post('/oficina','OficinaController@guardar');

$router->delete('/oficina/{id}','OficinaController@eliminar');

$router->post('/oficina/{id}','OficinaController@actualizar');