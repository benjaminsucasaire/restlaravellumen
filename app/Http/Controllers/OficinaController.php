<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Oficina;

class OficinaController extends Controller{
public function index(){
/*llamos a las datos de la tabla Oficina y leuego lo retornamos en formato json */
$datosOficina= Oficina::all();
return response()->json($datosOficina);
}

public function guardar(Request $request){
    /*creamos un objeto de la clase Oficina, e ingresamos los datos recividos por post meidiante parametro */
    $datosOficina= new Oficina;
    $datosOficina->nombre=$request->nombre;
    $datosOficina->pais=$request->pais;
    $datosOficina->departamento=$request->departamento;
    $datosOficina->provincia=$request->provincia;
    $datosOficina->distrito=$request->distrito;
    $datosOficina->telefono=$request->telefono;
    $datosOficina->estado=$request->estado;
    /*guardamos el objeto con todos los parametros*/
    $datosOficina->save();
    return response()->json($request);
    }


    public function ver($id){
    /*creamos un objeto de la clase Oficina, y buscamos por el id */
        $datosOficina= new Oficina;
        $datosEncontrados=$datosOficina->find($id);
        /*retornamos la fila con ese id*/
        return response()->json($datosEncontrados);
        }

        public function eliminar($id){
            /*creamos un objeto de la clase Oficina, y buscamos por el id para borrar */
                $datosOficina=  Oficina::find($id);

                /*si existe ese archivo, que lo borre*/
                if($datosOficina){
                    $datosOficina->delete();
                }

                /*retornamos la fila con ese id*/
                return response()->json("Registro borrado");
                }
            
        public function actualizar(Request $request, $id){
            /*creamos un objeto de la clase Oficina, y buscamos por el id para borrar */
             $datosOficina=  Oficina::find($id);
        
                        /*si existe ese archivo, que lo borre*/
            if($request->input(('nombre'))){
             $datosOficina->nombre=$request->input('nombre');
          }
          if($request->input(('pais'))){
            $datosOficina->pais=$request->input('pais');
         }
         if($request->input(('departamento'))){
            $datosOficina->departamento=$request->input('departamento');
         }
         if($request->input(('provincia'))){
            $datosOficina->provincia=$request->input('provincia');
         }
         if($request->input(('distrito'))){
            $datosOficina->distrito=$request->input('distrito');
         }
         if($request->input(('telefono'))){
            $datosOficina->telefono=$request->input('telefono');
         }
         
         /*este valor no se tiene que poner en una considicional, ya que si es verdadero y quieres cambiarlo a falso, no cambiara su valor inicial*/

            $datosOficina->estado=$request->input('estado');

         
         
          $datosOficina->save();
        
           /*retornamos la fila con ese id*/
           return response()->json($datosOficina);
         }        

}