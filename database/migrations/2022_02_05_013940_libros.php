<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Libros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toficina', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',700);
            $table->string('pais',70);
            $table->string('departamento',70);
            $table->string('provincia',70);
            $table->string('distrito',70);
            $table->string('telefono',20);
            $table->boolean('estado')->default(false);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toficina');
    }
}
